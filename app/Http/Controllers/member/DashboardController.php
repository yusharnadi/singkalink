<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $urls = DB::table('urls')->where('urls.user_id', Auth::id())->count();
        $hits = DB::table('visitors')->where('urls.user_id', Auth::id())
            ->join('urls', 'urls.short_url', '=', 'visitors.short_url')->count();

        return view('member.dashboard.index', ['url' => $urls, 'hits' => $hits]);
    }
}
