<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\URL;
use Jenssegers\Agent\Facades\Agent;

class UrlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $urls = DB::table('urls')->where('user_id', Auth::id())
            ->paginate('10');
        return view('member.url.index', ['urls' => $urls]);
    }

    static public function hits($short_url)
    {
        $hits = DB::table('visitors')->where('short_url', $short_url)->count();
        return $hits;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'full_url' => 'required|url',
        ]);

        if (!$this->_urlValidator($request->full_url)) {
            return back()->with('error', 'URL tidak valid / url server tujuan Down.');
        }
        $shortUrl = $this->_shortenUrl(4);
        // $shortUlr = 'FdMW';
        $shortUrlRef = DB::table('urls')->where('short_url', $shortUrl)->get();
        // dd($shortUrlRef);
        if (!$shortUrlRef->isEmpty()) {
            return back()->with('error', 'Kami mengalami gangguan, silahkan coba kembali.');
        }

        $is_guest = (Auth::id() == '') ? 1 : 0;
        $data = [
            'user_id' => Auth::id(),
            'full_url' => $request->full_url,
            'short_url' => $shortUrl,
            'is_guest' => $is_guest,
            'created_at' => now(),
            'updated_at' => now()
        ];

        DB::table('urls')->insert($data);
        return back()->with('message', URL::to('') . '/' . $shortUrl);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function delete($id)
    {

        DB::table('urls')->where('short_url', $id)->delete();
        DB::table('visitors')->where('short_url', $id)->delete();
        return back()->with('message', 'Link berhasil dihapus.');
    }

    private function _shortenUrl($limit)
    {
        $shortUlr = '';
        $char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
        for ($i = 0; $i < $limit; $i++) {
            $index = rand(0, (strlen($char) - 1));
            $shortUlr .= $char[$index];
        }

        return $shortUlr;
    }

    private function _urlValidator($uri)
    {

        try {
            //code...
            $client = Http::get($uri);
            if ($client) {
                return true;
            }
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function redirectTo(Request $request, $short_url)
    {
        $url = DB::table('urls')->where('short_url', $short_url)->first();
        if ($url == NULL) {
            return redirect()->route('home')->with('error', 'Url tidak ditemukan diserver kami.');
        }
        $data = [
            'short_url' => $short_url,
            'ip' => $request->ip(),
            'platform' => Agent::platform(),
            'browser_agent' => Agent::browser(),
            'created_at' => now(),
        ];

        DB::table('visitors')->insert($data);
        return redirect()->away($url->full_url);
    }
}
