<?php

namespace App\Http\Controllers\member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProfileController extends Controller
{
    public function index()
    {
        $profile = DB::table('users')->where('id', Auth::id())->first();
        return view('member.profile.index', ['profile' => $profile]);
    }

    public function update(Request $request)
    {
        # code...
        $request->validate([
            'name' => 'required|min:3'
        ]);

        $data = [
            'name' => $request->name,

        ];

        if ($request->email != Auth::user()->email) {
            # code...
            $request->validate([
                'email' => 'unique:users,email|email'
            ]);
        }
        $data = array_merge($data, ['email' => $request->email]);

        if (!empty($request->password)) {
            $request->validate([
                'password' => 'min:6'
            ]);
            $data =  array_merge($data, ['password' => $request->password]);
        }
        DB::table('users')->where('id', Auth::id())->update($data);
        return redirect()->route('member.profile.index')->with('message', 'Data profil berhasil diubah.');
    }
}
