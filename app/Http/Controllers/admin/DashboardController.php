<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function index()
    {
        $urls = DB::table('urls')->count();
        $hits = DB::table('visitors')
            ->join('urls', 'urls.short_url', '=', 'visitors.short_url')->count();

        return view('member.dashboard.index', ['url' => $urls, 'hits' => $hits]);
    }
}
