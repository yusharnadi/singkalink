<?php

namespace App\Http\Controllers;

use App\Mail\registrationMail;
use App\Mail\resetPasswordMail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class AuthController extends Controller

{
    public function index()
    {
        return view('auth.login');
    }
    public function login()
    {
        return view('auth.login');
    }

    public function doLogin(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);

        $ValidUser = DB::table('users')->where('email', $request->email)->first();

        if (!$ValidUser) {
            return redirect()->route('login')->with('error', 'error apa ya');
        }

        if ($ValidUser->email_verified_at == NULL) {
            return redirect()->route('login')->with('error', 'akun belum aktif, silahkan cek email anda');
        }

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $request->session()->put('login_time', now());
            return redirect()->route('member.dashboard.index');
        }

        return back()->with('error', 'Kombinasi Email dan Password salah.');;
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login');
    }

    public function register()
    {
        return view('auth.register');
    }

    public function doRegister(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6',
        ]);

        $data = [
            'name' => $request->name,
            'email' => $request->email,
            'level' => 2,
            'password' => bcrypt($request->password),
            'created_at' => now(),
            'remember_token' => Str::random(10)
        ];
        $link = route('verify', $data['remember_token']);
        // dd($link);
        DB::table('users')->insert($data);
        Mail::to($request->email)->send(new registrationMail($link));
        return redirect()->route('home')->with('message', 'Silahkan cek kotak masuk email / Kotak Spam anda untuk melanjutkan proses verifikasi.');
    }

    public function verify($link)
    {

        $data = ['email_verified_at' => now()];
        $userToken = DB::table('users')->where('remember_token', $link)->first();
        if ($userToken->remember_token != $link) {
            return redirect()->route('login')->with('error', 'Token tidak valid');
        } else {
            DB::table('users')->where('remember_token', $link)->update($data);
            return redirect()->route('login')->with('message', 'Akun anda berhasil di aktifkan.');
        }
    }

    public function reset()
    {
        return view('auth.reset');
    }

    public function doReset(Request $request)
    {
        $request->validate([
            'email' => 'email|required'
        ]);
        $user = DB::table('users')->where('email', $request->email)->first();
        if (empty($user)) {
            return redirect()->route('reset')->with('error', 'Email anda tidak terdaftar.');
        }

        $link = route('resetVerify', $user->remember_token);
        // dd($link);

        Mail::to($request->email)->send(new resetPasswordMail($link));
        return back()->with('message', 'Silahkan cek email anda, link reset telah kami kirimkan.');
    }

    public function resetVerify($token)
    {
        $tokenValid = DB::table('users')->where('remember_token', $token)->first();
        if (!empty($tokenValid)) {
            return view('auth.passwordForm', ['token' => $tokenValid->remember_token]);
        }
        return redirect()->route('login');
    }

    public function passwordReset(Request $request)
    {
        $request->validate([
            'password' => 'min:6|required',
            'remember_token' => 'required',
        ]);
        $data = ['password' => bcrypt($request->password)];
        DB::table('users')->where('remember_token', $request->remember_token)->update($data);

        return redirect()->route('login');
    }
}
