@extends('layouts.admin-master')
@section('page-heading')
    Dashboard
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-primary">
          <i class="fas fa-globe"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Total URL</h4>
          </div>
          <div class="card-body">
            {{$url}} 
          </div>
        </div>
      </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-12">
      <div class="card card-statistic-1">
        <div class="card-icon bg-warning">
          <i class="fas fa-chart-line"></i>
        </div>
        <div class="card-wrap">
          <div class="card-header">
            <h4>Total Hits</h4>
          </div>
          <div class="card-body">
            {{$hits}}
          </div>
        </div>
      </div>
    </div>                  
  </div>
  <div class="row">
    <div class="col">
      <form action="{{route('url.store')}}" role="form" method="POST">
        @csrf
        <div class="form-group mb-3">
            <div class="form-group">
              <label for="url-input" class="form-control-label">URL</label>
              <div class="input-group mb-3">
                <input class="form-control" type="url" name="full_url" id="url-input" placeholder="Paste / Tempel tautan link untuk menyingkat" value="https://" required="" autocomplete=""> 
                <div class="input-group-append">
                  <button type="submit" class="btn btn-warning">Singkatkan</button>
                </div>
              </div>
            </div>
          </div>
       </form>
       @if (session('error'))
       <div class="alert alert-danger">{{session('error')}}</div> 
       @endif
       @if (session('message'))
       <div class="alert alert-primary">{{session('message')}}</div> 
       @endif
       
      
    </div>
  </div>
@endsection