@extends('layouts.admin-master')
@section('page-heading')
    Ubah Profil
@endsection

@section('content')
  <div class="row">
    <div class="col col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                @if (session('message'))    
                <div class="alert alert-primary">
                    {{session('message')}}
                </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}
                            @endforeach
                    </div>
                @endif
                <form action="{{route('member.profile.update')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control col-md-4" name="email" value="{{$profile->email}}" required>
                    </div>
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" class="form-control col-md-4" name="name" value="{{$profile->name}}" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control col-md-4" name="password">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
@endsection