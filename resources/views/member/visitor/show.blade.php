@extends('layouts.admin-master')
@section('page-heading')
    Visitor Url
@endsection

@section('content')
  <div class="row">
    <div class="col col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body">
                <a href="{{url()->previous()}}" class="btn btn-primary mb-2"><i class="fas fa-arrow-left"></i> Kembali</a>
                <table class="table table-bordered table-hover table-md">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Shorten Url</th>
                            <th>Full Url</th>
                            <th>Access at</th>
                            <th>OS</th>
                            <th>Agent</th>
                            <th>IP Addr</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $no =1 @endphp
                        @foreach ($visitors as $url)
                            <tr>
                                <td>{{$visitors ->perPage()*($visitors->currentPage()-1)+$no++}}</td>
                                <td>{{URL::to('') . '/' .$url->short_url}}</td>
                                <td>{{$url->full_url}}</td>
                                <td>{{$url->created_at}}</td>
                                <td>
                                    {{-- {{$url->platform}} --}}
                                    @if ($url->platform == 'OS X')
                                        <i class="fab fa-apple"></i>
                                    @elseif ($url->platform == 'Linux')
                                    <i class="fab fa-linux"></i>
                                    @elseif ($url->platform == 'Ubuntu')
                                    <i class="fab fa-ubuntu"></i>
                                    @elseif ($url->platform == 'Windows')
                                    <i class="fab fa-windows"></i>
                                    @else
                                          {{$url->platform}}  
                                    @endif
                                </td>
                                <td>
                                    @if ($url->browser_agent == 'Chrome')
                                        <i class="fab fa-chrome"></i>
                                    @elseif ($url->browser_agent == 'Firefox')
                                    <i class="fab fa-firefox"></i>
                                    @elseif ($url->browser_agent == 'Edge')
                                    <i class="fab fa-edge"></i>
                                    @elseif ($url->browser_agent == 'Opera')
                                    <i class="fab fa-opera"></i>
                                    @elseif ($url->browser_agent == 'Safari')
                                    <i class="fab fa-safari"></i>
                                    @elseif ($url->browser_agent == 'IE')
                                    <i class="fab fa-internet-explorer"></i>
                                    @else
                                          {{$url->browser_agent}}  
                                    @endif
                                </td>
                                <td>{{$url->ip}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-body text-right">
                <nav class="d-inline-block">
                  {{$visitors->links()}}
                </nav>
            </div>
        </div>
    </div>
  </div>
@endsection