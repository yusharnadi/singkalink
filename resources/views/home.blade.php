<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  {{-- <title>Home &mdash; {{ config('app.name')}}</title> --}}
  <!-- Primary Meta Tags -->
  <title>Singka.link — url singkat generator</title>
  <meta name="title" content="Singka.link — url singkat generator">
  <meta name="description" content="Layanan pemendek url atau link gratis bebas iklan dan akses sangat cepat dari dinas kominfo kota singkawang. Anda dapat melihat statistik pengunjung link yang telah anda singkat.">

  <!-- Open Graph / Facebook -->
  <meta property="og:type" content="website">
  <meta property="og:url" content="https://singka.link">
  <meta property="og:title" content="Singka.link — url singkat generator">
  <meta property="og:description" content="Layanan pemendek url atau link gratis bebas iklan dan akses sangat cepat dari dinas kominfo kota singkawang. Anda dapat melihat statistik pengunjung link yang telah anda singkat.">
  <meta property="og:image" content="{{asset('assets/img/singkalink.png')}}">

  <!-- General CSS Files -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">


  <!-- Template CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
<style>
    #app {
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
        height: 100vh;
        background: url({{asset('assets/img/bg-tcm.jpg')}}) no-repeat center;
        background-size: cover;
        /* backdrop-filter: grayscale(20%); */
    }
    .main-wrapper, .main-footers {
        flex: 1;
    }
    .content {
        flex: 2;
    }
    .navbar {left: 0}
    .navbar-bg {height: 10% !important}
    .footer-right{
        display: flex;
        justify-content: center;
        align-items: center;
    }

    .main-footers {
        display: flex;
        align-items: center;
        justify-content: space-between;
        margin: 20px;
    }
    .footer-right > a >i {
        color: #AAB5BA;
        font-size: 16px;
    }
    .filter {
    /* flex: 1; */
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    background-color: rgba(0, 0, 0, 0.5);
    z-index: 0;
}
.footer-link {
    font-size: 1rem;
}
    
</style>
</head>
<body>
  <div id="app">
    <div class="filter">
    </div>
    <div class="main-wrapper">
        <div class="navbar-bg bg-warning"></div>
        <!-- navbar -->
        <nav class="navbar navbar-expand-lg main-navbar">
            <a href="index.html" class="navbar-brand "><img src="{{asset('assets/img/logo-skw-150.png')}}" alt="Logo Pemkot Singkawang" srcset="" height="60px"></a>
            <form class="form-inline ml-auto">
            </form>
            <ul class="navbar-nav navbar-right">
                <li class="nav-item active"><a href="{{route('login')}}" class="nav-link ">Masuk</a></li>
                <li class="nav-item active"><a href="{{route('register')}}" class="nav-link nav-link-lg ">Daftar</a></li>
            </ul>
          </nav>
    </div>
    {{-- CONTENT  --}}
    <div class="content container">
        <div class="row">
            <div class="card px-2 py-2 col col-md-8 offset-md-2">
                <div class="card-body">
                   <form action="{{route('home.store')}}" role="form" method="POST">
                    @csrf
                    <div class="form-group mb-3">
                        <div class="form-group">
                          <label for="url-input" class="form-control-label">URL</label>
                          <div class="input-group mb-3">
                            <input class="form-control form-control-lg" type="url" name="full_url" id="url-input" placeholder="Tempel tautan link untuk menyingkat." value="" required="" autocomplete=""> 
                            <div class="input-group-append">
                              <button type="submit" class="btn btn-warning">Singkatkan</button>
                            </div>
                          </div>
                        </div>
                      </div>
                   </form>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col col-md-8 offset-md-2">
            @if (session('error'))
              <div class="alert alert-danger">{{session('error')}}</div> 
            @endif
            @if (session('message'))
              <div class="alert alert-primary"><h6>{{session('message')}}</h6></div> 
            @endif
          </div>
        </div>
    </div>
    {{-- FOOTER  --}}
    <footer class="main-footers fixed-bottom">
            <div class="footer-left">
              Copyright © 2021 <div class="bullet"></div>  <a href="{{route('home')}}" class="footer-link">Singka.link</a>
            </div>
            <div class="footer-right">
                <a target="_blank" href="https://www.facebook.com/mc.singkawang" class="nav-link nav-link-lg"><i class="fab fa-facebook"></i></a>
                <a target="_blank" href="https://www.instagram.com/kominfo_singkawang/" class="nav-link nav-link-lg"><i class="fab fa-instagram"></i></a>
                <a target="_blank" href="https://www.youtube.com/channel/UCi1ghiT9lzGFjfSdU0fISIg" class="nav-link nav-link-lg"><i class="fab fa-youtube"></i></a>
                <a target="_blank" href="https://mobile.twitter.com/mckominfoskw1" class="nav-link nav-link-lg"><i class="fab fa-twitter"></i></a>
            </div>
          </footer>
    </div>
  

  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{ asset('assets/js/stisla.js')}}"></script>

  <!-- JS Libraies -->

  <!-- Template JS File -->
  <script src="../assets/js/scripts.js"></script>
  <script src="../assets/js/custom.js"></script>

  <!-- Page Specific JS File -->
</body>
</html>
