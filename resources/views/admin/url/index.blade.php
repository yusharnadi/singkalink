@extends('layouts.admin-master')
@section('page-heading')
    Kelola Url
@endsection

@section('content')
  <div class="row">
    <div class="col col-md-12 col-lg-12">
        <div class="card">
            <div class="card-body table-responsive">
                <table class="table table-bordered table-hover table-md">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>User</th>
                            <th>Full Url</th>
                            <th>Shorten Url</th>
                            <th>Hits</th>
                            <th>Created_at</th>
                            <th>#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php $no =1 @endphp
                        @php use \App\Http\Controllers\member\UrlController; @endphp
                        @foreach ($urls as $url)
                            <tr>
                                <td>{{$urls ->perPage()*($urls->currentPage()-1)+$no++}}</td>
                                <td>{{$url->email ? $url->email:'guest'}}</td>
                                <td>{{$url->full_url}}</td>
                                <td><a href="{{url('/').'/'.$url->short_url}}" target="_blank" rel="noopener noreferrer">{{url('/').'/'.$url->short_url}}</a></td>
                                <td>{{UrlController::hits($url->short_url)}}</td>
                                <td>{{$url->created_at}}</td>
                                <td class="d-flex">
                                    <a href="{{route('visitor.show', $url->short_url)}}" class="btn btn-primary btn-sm" data-toggle="tooltip" data-placement="top" title="" data-original-title="Lihat Detail"><i class="fas fa-align-center"></i></a>
                                    {{-- <a href="#" class="btn btn-warning btn-sm ml-1""><i class="fas fa-edit"></i></a>--}}
                                    <a href="{{route('member.urls.delete', $url->short_url)}}" class="btn btn-danger btn-sm ml-1"><i class="fas fa-trash"></i></a> 
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="card-body text-right">
                <nav class="d-inline-block">
                  {{$urls->links()}}
                </nav>
            </div>
        </div>
    </div>
  </div>
@endsection