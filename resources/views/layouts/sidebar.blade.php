<div class="main-sidebar">
    <aside id="sidebar-wrapper">
      <div class="sidebar-brand">
      <a href="{{route('member.dashboard.index')}}"><img class="mr-1" src="{{asset('assets/img/logo-skw-150.png')}}" alt="Logo Pemkot Singkawang" srcset="" height="50px">{{env('APP_NAME')}}</a>
      </div>
      <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{route('member.dashboard.index')}}">
          <img src="{{asset('assets/img/logo-skw-150.png')}}" alt="Logo Pemkot Singkawang" srcset="" height="50px">
        </a>
      </div>
      <ul class="sidebar-menu">
          <li class="menu-header">Main Menu</li>
          @can('admin')
          <li><a class="nav-link" href="{{ route('admin.dashboard.index') }}"><i class="fas fa-desktop"></i><span>Dashboard</span></a></li>
          <li><a class="nav-link" href="{{ route('adminUrl.index') }}"><i class="fas fa-globe"></i><span>Kelola URL</span></a></li>
          <li><a class="nav-link" href=""><i class="fas fa-user"></i><span>User</span></a></li>
          
          @elsecan('member')
          <li><a class="nav-link" href="{{ route('member.dashboard.index') }}"><i class="fas fa-desktop"></i><span>Dashboard</span></a></li>
          <li><a class="nav-link" href="{{ route('url.index') }}"><i class="fas fa-globe"></i><span>Kelola URL</span></a></li>
          <li><a class="nav-link" href="{{ route('member.profile.index') }}"><i class="fas fa-user"></i><span>Profile</span></a></li>
          @endcan
          </li>
        </ul>
        <div class="mt-4 mb-4 p-3 hide-sidebar-mini">
          <a href="{{ route('logout') }}" class="btn btn-danger btn-lg btn-block btn-icon-split">
            <i class="fas fa-sign-out-alt"></i> Logout
          </a>
        </div>
    </aside>
</div>