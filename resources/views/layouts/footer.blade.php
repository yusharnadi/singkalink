<footer class="main-footer">
    <div class="footer-left">
      Copyright &copy; 2021 <div class="bullet"></div> {{config('app.name')}} <a href="https://kominfo.singkawangkota.go.id/" target="_blank">{{config('app.dinas')}}</a>
    </div>
    <div class="footer-right">
      V 0.1
    </div>
</footer>