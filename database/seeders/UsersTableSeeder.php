<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $member = [
        //     'name' => 'Yusharnadi',
        //     'email' => 'yust.44@gmail.com',
        //     'level' => '2',
        //     'email_verified_at' => now(),
        //     'password' => bcrypt('yust#44'),
        //     'remember_token' => Str::random(10),
        // ];
        $admin = [
            'name' => 'Admin Pusat',
            'email' => 'admin@admin.com',
            'level' => '1',
            'email_verified_at' => now(),
            'password' => bcrypt('yust#44'),
            'remember_token' => Str::random(10),
        ];
        DB::table('users')->insert($admin);
    }
}
