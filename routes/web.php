<?php

use App\Http\Controllers\admin\DashboardController as AdminDashboardController;
use App\Http\Controllers\admin\UrlController as AdminUrlController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\member\ProfileController;
use App\Http\Controllers\member\UrlController;
use App\Http\Controllers\member\VisitorController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/{id}', [UrlController::class, 'redirectTo']);
Route::post('/home/store', [HomeController::class, 'store'])->name('home.store');


Route::group(['middleware' => 'guest'], function () {
    Route::get('/auth/login', [AuthController::class, 'index'])->name('login');
    Route::post('/auth/login', [AuthController::class, 'doLogin'])->name('doLogin');
    Route::get('/auth/register', [AuthController::class, 'register'])->name('register');
    Route::post('/auth/register', [AuthController::class, 'doRegister'])->name('doRegister');
    Route::get('/auth/verify/{link}', [AuthController::class, 'verify'])->name('verify');


    Route::get('/auth/resetVerify/{link}', [AuthController::class, 'resetVerify'])->name('resetVerify');
    Route::get('/auth/reset', [AuthController::class, 'reset'])->name('reset');
    Route::post('/auth/reset', [AuthController::class, 'doReset'])->name('doReset');
    Route::put('/auth/passwordReset', [AuthController::class, 'passwordReset'])->name('passwordReset');
});

Route::group(['middleware' => 'auth'], function () {
    Route::resource('/member/url', UrlController::class);
    Route::resource('/admin/adminUrl', AdminUrlController::class);
    Route::resource('/member/visitor', VisitorController::class);
    Route::get('/member/dashboard', [DashboardController::class, 'index'])->name('member.dashboard.index');
    Route::get('/admin/dashboard', [AdminDashboardController::class, 'index'])->name('admin.dashboard.index');
    Route::get('/member/profile', [ProfileController::class, 'index'])->name('member.profile.index');
    Route::post('/member/profile', [ProfileController::class, 'update'])->name('member.profile.update');
    Route::get('/auth/logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('/member/url/{short}/delete', [UrlController::class, 'delete'])->name('member.urls.delete');
});
